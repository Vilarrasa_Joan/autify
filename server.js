var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var methodOverride = require('method-override');
var session = require('express-session');
var passport = require('passport');
var path = require('path');
var http = require('http');
var SpotifyStrategy = require('passport-spotify').Strategy;
var consolidate = require('consolidate');
var SpotifyWebApi = require('spotify-web-api-node');
var LoggedInUsers = [];

var appKey = 'e5da77d265d54cdfb932d25a50cf71bb';
var appSecret = '981fbe71e8d3422ab07b9a7a3f9b6691';

// Passport session setup.
//   To support persistent login sessions, Passport needs to be able to
//   serialize users into and deserialize users out of the session. Typically,
//   this will be as simple as storing the user ID when serializing, and finding
//   the user by ID when deserializing. However, since this example does not
//   have a database of user records, the complete spotify profile is serialized
//   and deserialized.
passport.serializeUser(function(user, done) {
    done(null, user);
});
passport.deserializeUser(function(obj, done) {
    done(null, obj);
});

passport.use(new SpotifyStrategy({
    clientID: appKey,
    clientSecret: appSecret,
    callbackURL: 'http://localhost:8888/callback'
    },
    function(accessToken, refreshToken, expires_in, profile, done) {
        LoggedInUsers[profile.id] = {accessToken: accessToken, refreshToken: refreshToken};
        process.nextTick(function () {
        return done(null, profile);
    });
}));

var app = express();

// Importar les rutes de MongoDB
const api = require('./server/routes/api');

app.use(cookieParser());
app.use(bodyParser());
app.use(methodOverride());
app.use(session({ secret: 'keyboard cat' }));
// Initialize Passport!  Also use passport.session() middleware, to support
// persistent login sessions (recommended).
app.use(passport.initialize());
app.use(passport.session());

// Angular DIST output folder
app.use(express.static(path.join(__dirname, 'dist')));

// Ruta base de la API de Mongodb
app.use('/api', api);

// Login que redirigeix a spotify
app.get('/auth/spotify',
    passport.authenticate('spotify', {scope: ['user-read-private', 'user-read-email', 'playlist-modify-private', 'playlist-read-private', 'playlist-modify-public'], showDialog: true}),
    function(req, res){
});

// Rep el codi de spotify i demana els tokens.
app.get('/callback',
    passport.authenticate('spotify', { failureRedirect: 'http://localhost:8888' }),
    function(req, res) {
        res.redirect('http://localhost:8888');
});

app.get('/logout', function(req, res){
    delete LoggedInUsers[req.user.id];
    req.logout();
    req.session.destroy(function (err) {
        res.redirect('http://localhost:8888');
    });
});

/*SPOTIFY API*/

// Rebre de l'usuari que està loguejat.
app.get('/spotify/user', ensureAuthenticated, (req, res) => {
    res.json({ user: req.user._json });
});

// Playlists de l'usuari que esta loguejat
app.get('/spotify/user/playlists', ensureAuthenticated, (req, res) => {
    var spotifyApi = createSpotifyApiObject(req.user.id);
    spotifyApi.getUserPlaylists(req.user.id)
        .then(function(data) {
            let pFiltrades = [];
            data.body.items.forEach(playlist => {
                var playlistOwnerId = playlist.uri.split(':');
                playlistOwnerId = playlistOwnerId[2];
                if(playlistOwnerId == req.user.id)
                    pFiltrades.push(playlist);
            });
            res.json(pFiltrades);
        },function(err) {
            res.json(err);
    });
});

// Playlist (per id) de l'usuari que esta loguejat
app.get('/spotify/user/playlist/:playlistId', ensureAuthenticated, (req, res) => {
    var spotifyApi = createSpotifyApiObject(req.user.id);
    spotifyApi.getPlaylist(req.user.id, req.params.playlistId)
        .then(function(data) {
            res.json(data);
        }, function(err) {
            res.json(err);
    });
});

// Objecte de artista (buscat per la id)
app.get('/spotify/artist/:artistId', ensureAuthenticated, (req, res) => {
    var spotifyApi = createSpotifyApiObject(req.user.id);
    spotifyApi.getArtist(req.params.artistId)
        .then(function(data) {
            res.json(data);
        }, function(err) {
            res.json(err);
    });
});

// Àlbums de un artista (buscat per id)
app.get('/spotify/:artistId/albums', ensureAuthenticated, (req, res) => {
    var spotifyApi = createSpotifyApiObject(req.user.id);
    spotifyApi.getArtistAlbums(req.params.artistId)
        .then(function(data) {
            res.json(data);
        }, function(err) {
            res.json(err);
    });
});

// Tracks de un àlbum (buscat per id)
app.get('/spotify/:albumId/tracks', ensureAuthenticated, (req, res) => {
    var spotifyApi = createSpotifyApiObject(req.user.id);
    spotifyApi.getAlbumTracks(req.params.albumId, { limit : 50, offset : 0 })
        .then(function(data) {
            res.json(data);
        }, function(err) {
            res.json(err);
    });
});

// Afegir tracks a una playlist ed l'usuari loguejat
app.post('/spotify/:userId/:playlistId/addTracks', ensureAuthenticated, (req, res) => {
    var spotifyApi = createSpotifyApiObject(req.user.id);
    spotifyApi.addTracksToPlaylist(req.params.userId, req.params.playlistId, JSON.parse(req.body.tracks))
        .then(function(data) {
            res.json(data);
        }, function(err) {
            res.json(err);
    });
});

/*END SPOTIFY API*/

// Send all other requests to the Angular app
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});

//Set Port
app.set('port', '8888');

const server = http.createServer(app);

server.listen(8888, () => console.log('Running on localhost:8888'));

// Comprovació de que l'usuari està autenticat.
function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) { return next(); }
    res.redirect('http://localhost:8888');
}

function createSpotifyApiObject(idUser) {
    var spotifyApi = new SpotifyWebApi();
    spotifyApi.setAccessToken(LoggedInUsers[idUser].accessToken);
    spotifyApi.setRefreshToken(LoggedInUsers[idUser].refreshToken);
    spotifyApi.setRedirectURI('http://localhost:8888/callback');
    spotifyApi.setClientId(appKey);
    spotifyApi.setClientSecret(appSecret);
    return spotifyApi;
}
