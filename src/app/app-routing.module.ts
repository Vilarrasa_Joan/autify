import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CrearPlaylistComponent } from './components/crear-playlist/crear-playlist.component';
import { LlistaAutorsComponent } from './components/llista-autors/llista-autors.component';
import { LlistaPlaylistsComponent } from './components/llista-playlists/llista-playlists.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: LlistaPlaylistsComponent },
  { path: 'playlist/:id', component: CrearPlaylistComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
