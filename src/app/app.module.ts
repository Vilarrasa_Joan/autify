import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';


// Import the Http Module and our Data Service
import { HttpModule } from '@angular/http';
import { DataService } from './services/data.service';
import { SpotifyService } from './services/spotify.service';
import { AppRoutingModule } from './/app-routing.module';
import { LoginComponent } from './components/login/login.component';
import { LlistaAutorsComponent } from './components/llista-autors/llista-autors.component';
import { LlistaPlaylistsComponent } from './components/llista-playlists/llista-playlists.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HeaderComponent } from './components/header/header.component';
import { CrearPlaylistComponent } from './components/crear-playlist/crear-playlist.component';
import { FooterComponent } from './components/footer/footer.component';
import { FaqComponent } from './components/faq/faq.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LlistaAutorsComponent,
    LlistaPlaylistsComponent,
    DashboardComponent,
    HeaderComponent,
    CrearPlaylistComponent,
    FooterComponent,
    FaqComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [DataService, SpotifyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
