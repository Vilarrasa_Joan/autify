import { Injectable } from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {

  result: any;

  user: any;

  constructor(private _http: Http) { }

  // Retorna els usuaris de la BBDD.
  getUsers() {
    return this._http.get('/api/users')
      .map(result => this.result = result.json().data);
  }

  // Retorna un usuari de la BBDD (buscat per id de spotify).
  getUser(id) {
    return this._http.get('/api/user/' + id)
      .map(result => this.user = result.json());
  }

  // Afegeix un usuari a la BBDD amb la id de spotify.
  postUser(id) {
    this._http.post('/api/user', { user_id: id } )
      .subscribe(res => {  });
  }

  // Afegeix una playlist a un usuari de la BBDD.
  postPlaylist(idUser, idPlaylist) {
    this._http.post('/api/' + idUser + '/playlist', { playlist_id: idPlaylist } )
      .subscribe(res => {  });
  }

  // Afegeix un artista a una playlist de un usuari de la BBDD.
  postArtist(idUser, idPlaylist, idArtist) {
    this._http.post('/api/' + idUser + '/' + idPlaylist + '/artist', { artist_id: idArtist } )
      .subscribe(res => {  });
  }

  // Elimina un artista de una playlist de un usuari de la BBDD.
  deleteArtist(idUser, idPlaylist, idArtist) {
    this._http.delete('/api/' + idUser + '/' + idPlaylist + '/' + idArtist )
      .subscribe(res => {  });
  }

  // Actualitzar la última data de actualització de un usuari de la BBDD a el temps actual.
  updateUserDate(idUser) {
    this._http.get('/api/user/' + idUser + '/updateDate')
      .subscribe(res => {  });
  }

  // Actualitzar la última data de actualització de un usuari de la BBDD el un dia concret.
  updateUserDateCustom(idUser, data) {
    this._http.post('/api/user/' + idUser + '/updateDate', { data: data })
      .subscribe(res => {  });
  }

}
