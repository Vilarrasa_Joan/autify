import { Injectable } from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class SpotifyService {

  user: any;

  artist: any;

  playlists: any;

  playlist: any;

  albums: any;

  tracks: any;

  constructor(private _http: Http) { }

  // Usuari loguejat a spotify.
  getUser() {
    return this._http.get('/spotify/user')
      .map(user => this.user = user.json().user);
  }

  // Playlists de l'usuari que esta loguejat
  getPlaylists() {
    return this._http.get('/spotify/user/playlists')
      .map(playlists => this.playlists = playlists.json());
  }

  // Playlist concreta de spotify
  getPlaylist(id) {
    return this._http.get('/spotify/user/playlist/' + id)
      .map(playlist => this.playlist = playlist.json().body);
  }

  // Artista de spotify
  getArtist(id) {
    return this._http.get('/spotify/artist/' + id)
      .map(artist => this.artist = artist.json().body);
  }

  // Àlbums de un artista de spotify
  getArtistAlbums(idArtist) {
    return this._http.get('/spotify/' + idArtist + '/albums')
      .map(albums => this.albums = albums.json().body);
  }

  // Tracks de un àlbum de spotify
  getAlbumTracks(idAlbum) {
    return this._http.get('/spotify/' + idAlbum + '/tracks')
      .map(tracks => this.tracks = tracks.json().body);
  }

  // Afegir tracks a una playlist de l'usuari loguejat de spotify.
  postTracksToPlaylist(idUser, idPlaylist, tracks) {
    this._http.post('/spotify/' + idUser + '/' + idPlaylist + '/addTracks', { tracks: tracks })
      .subscribe(res => {  });
  }

}
