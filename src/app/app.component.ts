import { Component } from '@angular/core';

// Import the SpotifyService
import { SpotifyService } from './services/spotify.service';
// Import the MongoDBService
import { DataService } from './services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  user: any;

  constructor(private _spotifyService: SpotifyService, private _dataService: DataService) {
    this._spotifyService.getUser().subscribe(res => {
      this.user = res;  // Assignar l'usuari a la variable.
      this._dataService.getUser(res.id).subscribe(usuari => {
        if (usuari.data.length === 0) { // Comprovar si l'usuari és o no a la BBDD
          this.insertaNouUsuari(res.id); // Insertar el nou usuari i les seves playlists a la BBDD
        } else {
          const playlistsUser = usuari.data[0].playlists; // playlists del user a la BBDD
          this._spotifyService.getPlaylists().subscribe(playlists => {  // playlists del user a Spotify
            playlists.forEach(playlist => {
              const i = this.buscaPlaylistBBDD(playlist, playlistsUser);
              if (i !== -1) {
                playlistsUser[i].artists.forEach(artist => {
                  this.actualitzaArtistPlaylist(usuari, playlist, artist);
                });
              } else {
                this._dataService.postPlaylist(res.id, playlist.id);    // Afegir la playlist (nova) en cas que no sigui a la BBDD
              }
            });
          });
          this._dataService.updateUserDate(res.id); // Actualitzar la última data de actualització.
        }
      });
    });
  }

  // Inserta un nou usuari a la BBDD
  insertaNouUsuari(userId) {
    this._dataService.postUser(userId);
    this._spotifyService.getPlaylists().subscribe(playlists => {
      playlists.forEach(playlist => {
        if (playlist.owner.id === userId) { // L'usuari ha de ser propietari de la playlist
          this._dataService.postPlaylist(userId, playlist.id);  // Afegir les playlists a la BBDD
        }
      });
    });
  }

  // Busca si un objecte playlist de spotify és o no a la BBDD.
  buscaPlaylistBBDD(playlist, playlistsUser) {
    let i = 0;
    while (i < playlistsUser.length) {
      if (playlistsUser[i].id_playlist === playlist.id) {
        return i;
      }
      i++;
    }
    return -1;
  }

  afegirTracksPlaylist(userId, playlistId, albumId) {
    this._spotifyService.getAlbumTracks(albumId).subscribe(tracks => {
      const tracksToAdd = []; // Crearem un array amb les tracks de l'àlbum per afegir-les a la playlist
      tracks.items.forEach(track => {
        tracksToAdd.push('spotify:track:' + track.id);
      }); // Un cop creada l'array només falta fer la crida per afegit les tracks a la playlist
      this._spotifyService.postTracksToPlaylist(userId, playlistId, JSON.stringify(tracksToAdd));
    });

  }

  actualitzaArtistPlaylist(usuari, playlist, artist) {
    this._spotifyService.getArtistAlbums(artist).subscribe(albums => { // Trobem els últims 50 àñbums de l'artista
      let trobatLimitData = false;  // Quan trobem un àlbum més antic que la última actualització pararem el loop
      this._spotifyService.getPlaylist(playlist.id).subscribe(realPlaylist => {
        for (let j = 0; j < albums.items.length && !trobatLimitData; j++) {
          if (new Date(albums.items[j].release_date) < new Date(usuari.data[0].last_updated)) {
            trobatLimitData = true; // L'Àlbum és més antic que l'actualització, parem el loop.
          }
          for (let k = 0; k < realPlaylist.tracks.items.length && !trobatLimitData; k++) {
            if (realPlaylist.tracks.items[k].track.album.id === albums.items[j].id) {
              trobatLimitData = true;
            }
          }
          if (!trobatLimitData) {
            this.afegirTracksPlaylist(usuari.data[0].id_user, playlist.id, albums.items[j].id);
          }
        }
      });
    });
  }

}
