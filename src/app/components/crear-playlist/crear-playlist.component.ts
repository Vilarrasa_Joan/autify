import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

// Import the SpotifyService
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-crear-playlist',
  templateUrl: './crear-playlist.component.html',
  styleUrls: ['./crear-playlist.component.css']
})
export class CrearPlaylistComponent implements OnInit {

  playlist: any;
  playlistName: any;
  playlistUrl: any;

  constructor(
    private route: ActivatedRoute,
    private _spotifyService: SpotifyService,
    private location: Location ) {}

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id'); // Llegir la id de la playlist de la url
    this._spotifyService.getPlaylist(id).subscribe(res => {
      this.playlist = res.tracks.items;
      this.playlistName = res.name;
      this.playlistUrl = res.external_urls.spotify;
    });
  }

}
