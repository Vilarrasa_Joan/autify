import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LlistaAutorsComponent } from './llista-autors.component';

describe('LlistaAutorsComponent', () => {
  let component: LlistaAutorsComponent;
  let fixture: ComponentFixture<LlistaAutorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LlistaAutorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LlistaAutorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
