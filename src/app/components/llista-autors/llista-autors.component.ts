import { DataService } from './../../services/data.service';
import { SpotifyService } from './../../services/spotify.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-llista-autors',
  templateUrl: './llista-autors.component.html',
  styleUrls: ['./llista-autors.component.css']
})
export class LlistaAutorsComponent implements OnInit {

  playlist: any;
  autors = [];
  autorsSeguits = [];
  allArtists = false;

  constructor(
    private route: ActivatedRoute,
    private _spotifyService: SpotifyService,
    private _dataService: DataService,
    private location: Location) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.getFollowedArtists(id);
    this.getPlaylistArtists(id);
  }

  // Funció que busca un artista a una llista de artistes
  containsArtist(artist, artistList) {
    for (let i = 0; i < artistList.length; i++) {
      if (artistList[i].id === artist.id) {
        return true;
      }
    }
    return false;
  }

  // Canviar l'status de un autor de no seguit a seguit o a la inversa
  changeStatus(id) {
    let followed = false;
    this.autorsSeguits.forEach(autorId => { // Buscar l'artista dins l'array ed artistes seguits
      if (autorId === id) {
        this._dataService.deleteArtist(this.playlist.owner.id, this.playlist.id, id); // Eliminar de la BBDD
        this.autorsSeguits.splice(this.autorsSeguits.indexOf(autorId), 1); // Eliminar de l'array
        followed = true; // Evitar que el segueixi la funció més tard
        this.autors.forEach(autor => { // Modificar la variable en l'autor del DOM
          if (autor.id === id) {
            autor.followed = false;
          }
        });
      }
    });
    if (!followed) {
      this._dataService.postArtist(this.playlist.owner.id, this.playlist.id, id); // Afegir a la BBDD
      this.autorsSeguits.push(id); // Afegir a l'array
      this.autors.forEach(autor => {  // Modificar la variable en l'autor del DOM
        if (autor.id === id) {
          autor.followed = true;
        }
      });
    }
  }

  // Trobar la imatge de l'artista
  getArtistImage(artist) {
    this._spotifyService.getArtist(artist.id).subscribe(resp => {
      if (resp.images.length === 0) {
        artist.img = 'assets/img/sol_key.jpg';
      } else {
        artist.img = resp.images[0].url;
      }
    });
  }

  // Marcar els autors que seguim
  markFollowedArtists() {
    this.autorsSeguits.forEach(idAutor => {
      this.autors.forEach(autor => {
        if (autor.id === idAutor) {
          autor.followed = true;
        }
      });
    });
  }

  // Trobar i marcar els artistes que es segueixen de la playlist
  getFollowedArtists(id) {
    this._spotifyService.getUser().subscribe(res => { // Recuperar l'objecte user de spotify
      this._dataService.getUser(res.id).subscribe(usuari => { // Recuperar l'objecte usuari de la BBDD
        let trobat = false; // Trobar els autors de la playlists que son a la BBDD (seguits)
        for (let i = 0; i < usuari.data[0].playlists.length && !trobat; i++) {
          if (usuari.data[0].playlists[i].id_playlist === id) {
            this.autorsSeguits = usuari.data[0].playlists[i].artists;
            trobat = true;
          }
        }
      });
    });
  }

  // Aconseguir els artistes de la playlist
  getPlaylistArtists(id) {
    this._spotifyService.getPlaylist(id).subscribe(res => {
      this.playlist = res; // Recuperar l'Objecte playlist de spotify
      this.playlist.tracks.items.forEach(track => { // Per a cada Track comprovar si l'artista és a l'array de artistes
        track.track.artists.forEach(artist => {
          if (!this.containsArtist(artist, this.autors)) {
            this.getArtistImage(artist);
            this.autors.push(artist); // Afegir artista a l'array de artistes
          }
        });
      });
      this.markFollowedArtists();
    });
  }

  // Modificar si es veuen tots o només una part dels artistes
  viewArtists() {
    if (this.allArtists === false) {
      this.allArtists = true;
    } else {
      this.allArtists = false;
      window.scroll({
        top: 0,
        left: 0,
        behavior: 'smooth'
      });
    }
  }

}
