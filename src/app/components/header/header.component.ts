import { Component, OnInit } from '@angular/core';

// Import the SpotifyService
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  user: any;
  userImg: any;
  userName: any;

  constructor(private _spotifyService: SpotifyService) { }

  ngOnInit() {
    this._spotifyService.getUser().subscribe(res => {
      this.user = res;
      if (this.user.images.length === 0) {
        this.userImg = 'assets/img/default-avatar.svg';
      } else {
        this.userImg = this.user.images[0];
      }
      if (this.user.display_name) {
        this.userName = this.user.display_name;
      } else {
        this.userName = this.user.id;
      }
    });
  }

}
