import { Component, OnInit } from '@angular/core';

// Import the SpotifyService
import { SpotifyService } from '../../services/spotify.service';
// Import the DataService
import { DataService } from './../../services/data.service';

@Component({
  selector: 'app-llista-playlists',
  templateUrl: './llista-playlists.component.html',
  styleUrls: ['./llista-playlists.component.css']
})
export class LlistaPlaylistsComponent implements OnInit {

  playlists: any;
  playlistImg: any;
  user: any;
  data: any;

  constructor(private _spotifyService: SpotifyService, private _dataService: DataService) { }

  ngOnInit() {
    // Usuari
    this._spotifyService.getUser().subscribe(res => this.user = res);
    // Playlists de l'usuari
    this._spotifyService.getPlaylists().subscribe(res => {
      this.playlists = res;
      this.playlists.forEach(playlist => {
        if (playlist.images.length === 0) {
          playlist.images.push ({
            height: 640,
            url:  'assets/img/default_cover_art.jpg',           ​​​​
            width: 640
          });
        }
      });
    });
  }

  irCrearPlaylist() {
    window.open('https://open.spotify.com/collection/playlists', '_blank');
  }

  updatePlaylists() {
    this._dataService.updateUserDateCustom(this.user.id, this.data);
    window.location.reload();
  }

}
