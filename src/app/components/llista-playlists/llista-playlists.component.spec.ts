import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LlistaPlaylistsComponent } from './llista-playlists.component';

describe('LlistaPlaylistsComponent', () => {
  let component: LlistaPlaylistsComponent;
  let fixture: ComponentFixture<LlistaPlaylistsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LlistaPlaylistsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LlistaPlaylistsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
