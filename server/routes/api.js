const express = require('express');
const router = express.Router();
const MongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;

// Connectar-se a la BBDD
const connection = (closure) => {
    return MongoClient.connect('mongodb://localhost:27017/autify', (err, client) => {
        if (err) return console.log(err);
        var db = client.db('autify');
        closure(db);
    });
};

// Error handling
const sendError = (err, res) => {
    response.status = 501;
    response.message = typeof err == 'object' ? err.message : err;
    res.status(501).json(response);
};

// Constructor de respostes
let response = {
    status: 200,
    data: [],
    message: null
};

// Get users
// Recupera els usuaris de BBDD
router.get('/users', (req, res) => {
    connection((db) => {
        db.collection('user')
            .find()
            .toArray()
            .then((users) => {
                response.data = users;
                res.json(response);
            })
            .catch((err) => {
                sendError(err, res);
            });
    });
});

// Get user
// Recupera un usuari de la BBDD buscar per la id de spotify
router.get('/user/:userId', (req, res) => {
    connection((db) => {
        db.collection('user')
            .find({id_user: req.params.userId})
            .toArray()
            .then((user) => {
                response.data = user;
                res.json(response);
            })
            .catch((err) => {
                sendError(err, res);
            });
    });
});

// Insert new user
// Insereix un nou usuari a la BBDD sense playlists i segons la id de spotify
router.post('/user', (req, res) => {
    connection((db) => {
        db.collection('user').insert({
            id_user: req.body.user_id,
            last_updated: new Date(),
            playlists: [],
       });
    });
});

// Insert new playlist
// Inserir una playlist a la BBDD a un usuari (buscar per la id de Spotify)
router.post('/:userId/playlist', (req, res) => {
    connection((db) => {
        db.collection('user')
            .update({id_user: req.params.userId},
                {$push: {playlists: {id_playlist: req.body.playlist_id, artists: []}}},
                {safe: true, upsert: true},
                function(err, doc) {
                    if(err){
                        console.log(err);
                    }else{
                        // Playlist insertada correctament
                    }
                });
    });
});

// Afegir un artista a seguir a una playlist de un usuari (buscats per les ids de spotify)
router.post('/:userId/:playlistId/artist', (req, res) => {
    connection((db) => {
        db.collection('user')
            .update({
                "id_user": req.params.userId,
                "playlists.id_playlist": req.params.playlistId },
                { "$push": { "playlists.$.artists": req.body.artist_id }},
                {safe: true, upsert: true},
                function(err, doc) {
                    if(err){
                        console.log(err);
                    }else{
                        // Artista insertada correctament
                    }
                });
    });
});

// Eliminar un artista de una playlist de un usuari (buscats per les ids de spotify)
router.delete('/:userId/:playlistId/:artistId', (req, res) => {
    connection((db) => {
        db.collection('user')
            .update({
                "id_user": req.params.userId,
                "playlists.id_playlist": req.params.playlistId },
                { $pull: { "playlists.$.artists": req.params.artistId }},
                {safe: true, upsert: true},
                function(err, doc) {
                    if(err){
                        console.log(err);
                    }else{
                        // Artista insertada correctament
                    }
                });
    });
});

// Actualitzar la última data de actualització de una usuari a la data d'avui
router.get('/user/:userId/updateDate', (req, res) => {
    connection((db) => {
        db.collection('user')
            .update({id_user: req.params.userId},
                { $set: { last_updated: new Date() } },
                { upsert: true })
        })
});

// Actualitzar la última data de actualització de una usuari a la data que ens demani
router.post('/user/:userId/updateDate', (req, res) => {
    connection((db) => {
        db.collection('user')
            .update({id_user: req.params.userId},
                { $set: { last_updated: new Date(req.body.data) } },
                { upsert: true })
        })
});

module.exports = router;
