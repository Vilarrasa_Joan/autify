# Autify

### Enlla�

  [www.autify.es](http://www.autify.es)

### Descripci�

- �s una aplicaci� web que permet connectar-te amb el teu compte de Spotify i crear llistes d'artistes que s'actualitzaran autom�ticament quan els artistes afegeixin nous tracks.

### Tecnologies 

* [Angular](https://angular.io)
* [Node.js](https://nodejs.org/es/)
* [MongoDB](https://www.mongodb.com)
* [HTML5](https://www.w3schools.com/html/html5_intro.asp)
* [CSS3](https://www.w3schools.com/css/default.asp)
* [Bootsrap](https://getbootstrap.com)
* [SpotifyAPI](https://beta.developer.spotify.com/documentation/web-api/)

### Moqup

* [Moqups](https://app.moqups.com/a16davmorgim@iam.cat/knXDIDzpvY/view)

### Desenvolupadors

* Joan Vilarrasa Cub�
* Pol Rod�s Ribote
* David Moreno Jim�nez

### Presentacions
* [Presentaci� Resum (EN)](https://prezi.com/view/z7yHC4yRM1CZMv8rAQNC/)
* [Presentaci� Resum (CA)](https://prezi.com/view/McndUBpT8MqC1pubHS3F/)
* [Presentaci� Talent Day](https://prezi.com/view/C9MZybCZk6A0qtE1jx6o/)
* [Presentaci� Comercial/Funcional](https://prezi.com/view/H1WKA8LSWz9b6fuVsfHL/)
* [Presentaci� T�cnica](https://prezi.com/view/ZsWpcNw6dKRDqSltHNGR/)